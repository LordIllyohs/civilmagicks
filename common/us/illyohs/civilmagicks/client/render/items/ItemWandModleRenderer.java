package us.illyohs.civilmagicks.client.render.items;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.opengl.GL11;

import us.illyohs.civilmagicks.client.models.item.ModelWand;
import us.illyohs.civilmagicks.common.lib.LibAsset;
import us.illyohs.civilmagicks.common.lib.LibInfo;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 31, 2014 at 8:07 PM.
 */
public class ItemWandModleRenderer implements IItemRenderer {

    private ModelWand model;

    public ItemWandModleRenderer() {
        model = new ModelWand();
    }
    private static final ResourceLocation TEXTURE = new ResourceLocation(LibInfo.MOD_ID, LibAsset.MODEL_BASE_WAND);

    @Override
    public boolean handleRenderType(ItemStack item, IItemRenderer.ItemRenderType type) {
        switch(type) {

            case ENTITY:
            case EQUIPPED_FIRST_PERSON:
            case EQUIPPED:
            case INVENTORY: return true;
            default: return false;
        }
    }

    @Override
    public boolean shouldUseRenderHelper(IItemRenderer.ItemRenderType type, ItemStack item, IItemRenderer.ItemRendererHelper helper) {
        return true;
    }

    @Override
    public void renderItem(IItemRenderer.ItemRenderType type, ItemStack item, Object... data) {
        switch(type) {
            case EQUIPPED: {
                GL11.glPushMatrix();

                GL11.glRotatef(20F, 0.0F, 0.0F, 20.0F);
                GL11.glRotatef(29F, 5.0F, 0.0F, 0.0F);
                GL11.glTranslatef(0.5F, 0.2F, -0.1F);
                GL11.glScalef(1.5F, 1.5F, 1.5F);
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(TEXTURE);
                model.render(.0625F);
                GL11.glPopMatrix();
                break;
            }
            case EQUIPPED_FIRST_PERSON: {
                GL11.glPushMatrix();

                GL11.glRotatef(5F, 0.0F, 0.0F, 20.0F);
                GL11.glRotatef(60F, 5.0F, 0.0F, 0.0F);
                GL11.glTranslatef(0.5F, 0.2F, -0.1F);
                GL11.glScalef(1.5F, 1.5F, 1.5F);
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(TEXTURE);
                model.render(.0625F);
                GL11.glPopMatrix();
                break;
            }
            case ENTITY: {
                GL11.glPushMatrix();

                GL11.glRotatef(5F, 0.0F, 0.0F, 20.0F);
                GL11.glRotatef(57F, 5.0F, 0.0F, 0.0F);
                GL11.glTranslatef(0.3F, 0.0F, -0.0F);
                GL11.glScalef(0.9F, 0.9F, 0.9F);
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(TEXTURE);
                model.render(.0625F);
                GL11.glPopMatrix();
                break;
            }
            case INVENTORY: {//LordIllyohs: I was a little lazy when modeling this while either fix in the renderer or remodel the rod not sure yet
                GL11.glPushMatrix();

                GL11.glRotatef(5F, 0.0F, 0.0F, 20.0F);
                GL11.glRotatef(57F, 5.0F, 0.0F, 0.0F);
                GL11.glTranslatef(0.5F, 0.2F, -0.1F);
                GL11.glScalef(0.5F, 0.9F, 0.9F);
                FMLClientHandler.instance().getClient().renderEngine.bindTexture(TEXTURE);
                model.render(.0625F);
                GL11.glPopMatrix();
                break;
            }
            default: break;

        }

    }
}
