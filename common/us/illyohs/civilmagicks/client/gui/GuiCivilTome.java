package us.illyohs.civilmagicks.client.gui;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ResourceLocation;

import us.illyohs.civilmagicks.common.lib.LibAsset;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 31, 2014 at 5:30 PM.
 */
public class GuiCivilTome extends GuiScreen {

    int xSize = 214;
    int ySize = 180;
    int left, top;

    private static final ResourceLocation TEXTURE = new ResourceLocation(LibAsset.CIVILTOME);

    @Override
    public void initGui() {
        super.initGui();
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {

    }

    public boolean doesGuiPauseGame() {
        return false;
    }


}