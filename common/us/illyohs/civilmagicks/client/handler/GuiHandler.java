package us.illyohs.civilmagicks.client.handler;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

import us.illyohs.civilmagicks.client.gui.GuiCivilTome;
import us.illyohs.civilmagicks.client.lib.LibGuiId;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 31, 2014 at 4:39 PM.
 */
public class GuiHandler implements IGuiHandler {

    public GuiHandler(){}

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == LibGuiId.BOOK_CIVILTOME) {
            return new GuiCivilTome();
        }
        return null;
    }
}
