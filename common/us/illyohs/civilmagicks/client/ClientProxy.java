package us.illyohs.civilmagicks.client;

import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import us.illyohs.civilmagicks.client.render.items.ItemWandModleRenderer;
import us.illyohs.civilmagicks.common.items.ModItems;
import us.illyohs.civilmagicks.common.lib.IProxy;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 30, 2014 at 7:47 PM.
 */
public class ClientProxy implements IProxy {

    @Override
    public void renderModels() {
        MinecraftForgeClient.registerItemRenderer(ModItems.baseWand, (IItemRenderer)new ItemWandModleRenderer());
    }

    @Override
    public void registerTiles() {
        // NO-OP
    }
}
