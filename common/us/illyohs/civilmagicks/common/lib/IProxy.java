package us.illyohs.civilmagicks.common.lib;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 30, 2014 at 7:46 PM.
 */
public interface IProxy {

    public abstract void renderModels();

    public  abstract void registerTiles();

}
