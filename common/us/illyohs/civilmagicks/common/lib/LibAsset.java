package us.illyohs.civilmagicks.common.lib;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on June 02, 2014 at 3:06 AM.
 */
public class LibAsset {

    //Roots
    private static final String ASSET = LibInfo.MOD_ID.toLowerCase();
    private static final String BLIT = ASSET + ":";
    private static final String GUI = ASSET + ":textures/gui/";
    private static final String MODELS = "textures/models/";
    private static final String FXASSETS = "textures/fx/";

    //Blocks
    public static final String TEMP = BLIT + "temp";

    //Items

    //GUI
    public static final String CIVILTOME = GUI + "civiltome.png";


    //Models
    public static final String MODEL_BASE_WAND = MODELS + "wand.png";

}
