package us.illyohs.civilmagicks.common.lib;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

import us.illyohs.civilmagicks.common.items.ModItems;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 31, 2014 at 1:24 AM.
 */
public class TabCivilMagicks extends CreativeTabs {

    public TabCivilMagicks(String s) {
        super(s);
    }

    @Override
    public Item getTabIconItem() {
        return ModItems.civilTome;
    }
}
