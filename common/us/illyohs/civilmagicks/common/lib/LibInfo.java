package us.illyohs.civilmagicks.common.lib;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 30, 2014 at 7:43 PM.
 */
public class LibInfo {

    public static final String MOD_ID = "civilmagicks";
    public static final String MOD_NAME = "Civil Magicks";
    public static final String MOD_VERSION = "@VERSION@";

    public static final String COMMONPROXY = "us.illyohs.civilmagicks.common.CommonProxy";
    public static final String CLIENTPROXY = "us.illyohs.civilmagicks.client.ClientProxy";
}
