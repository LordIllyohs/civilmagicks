package us.illyohs.civilmagicks.common;

import us.illyohs.civilmagicks.common.lib.IProxy;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 30, 2014 at 7:46 PM.
 */
public class CommonProxy implements IProxy{

    @Override
    public void renderModels() {
        //NO-OP
    }

    @Override
    public void registerTiles() {

    }
}
