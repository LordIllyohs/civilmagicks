package us.illyohs.civilmagicks.common.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;

import us.illyohs.civilmagicks.common.blocks.flames.*;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 31, 2014 at 12:24 AM.
 */
public class ModBlocks {
    
    public static Block brightFlame;
    
    public static void init() {
        
        brightFlame = new BlockBrightFlame();
        
        GameReg();
        
    }

    private static void GameReg() {

        GameRegistry.registerBlock(brightFlame, "brightflame");

    }
}
