package us.illyohs.civilmagicks.common.blocks.flames;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;

import us.illyohs.civilmagicks.CivilMagicks;
import us.illyohs.civilmagicks.common.lib.LibInfo;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 31, 2014 at 12:40 AM.
 */
public class BlockBrightFlame extends Block {

    public BlockBrightFlame() {
        super(Material.rock);
        this.setBlockName(LibInfo.MOD_ID + ":brightflame");
        this.setCreativeTab(CivilMagicks.tabCivV);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister reg) {
        blockIcon = reg.registerIcon(LibInfo.MOD_ID + ":temp");
    }

}
