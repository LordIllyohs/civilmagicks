package us.illyohs.civilmagicks.common.helper;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import us.illyohs.civilmagicks.common.lib.LibInfo;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 30, 2014 at 10:58 PM.
 */
public class LogHelper {

    private static final Logger CLog = LogManager.getLogger(LibInfo.MOD_ID);

    public static void log(Level logLevel, Object obj) {
        CLog.log(logLevel, obj.toString());
    }

    public static void debug(Object obj) {
        log(Level.DEBUG, obj.toString());
    }

    public static void warn(Object obj) {
        log(Level.WARN, obj.toString());
    }

    public static void info(Object obj) {
        log(Level.INFO, obj.toString());
    }

    public static void fatal(Object obj) {
        log(Level.INFO, obj.toString());
    }
}