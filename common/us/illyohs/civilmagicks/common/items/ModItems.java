package us.illyohs.civilmagicks.common.items;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import us.illyohs.civilmagicks.common.items.books.ItemCivilTome;
import us.illyohs.civilmagicks.common.items.wands.ItemBaseWand;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 30, 2014 at 8:45 PM.
 */
public class ModItems {

    //Books
    public static Item civilTome;

    //Wands
    public static Item baseWand;

    public static void init() {

        civilTome = new ItemCivilTome();

        baseWand = new ItemBaseWand();

        GameReg();
    }

    private static void GameReg() {
        GameRegistry.registerItem(civilTome, "civilTome");
        GameRegistry.registerItem(baseWand, "civilwandbasic");
        GameRegistry.addShapedRecipe(new ItemStack(civilTome), new Object[]{"ccc", "cxc", "ccc", 'c', Items.stick, 'x', Items.book});
    }
}
