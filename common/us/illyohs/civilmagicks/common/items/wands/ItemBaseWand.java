package us.illyohs.civilmagicks.common.items.wands;

import net.minecraft.item.Item;

import us.illyohs.civilmagicks.CivilMagicks;
import us.illyohs.civilmagicks.common.lib.LibInfo;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 31, 2014 at 8:20 PM.
 */
public class ItemBaseWand extends Item {

    public ItemBaseWand() {
        super();
        this.setCreativeTab(CivilMagicks.tabCivV);
        this.setUnlocalizedName(LibInfo.MOD_ID + ":basicwand");
    }
}
