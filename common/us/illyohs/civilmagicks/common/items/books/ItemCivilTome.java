package us.illyohs.civilmagicks.common.items.books;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import us.illyohs.civilmagicks.CivilMagicks;
import us.illyohs.civilmagicks.client.lib.LibGuiId;
import us.illyohs.civilmagicks.common.helper.LogHelper;
import us.illyohs.civilmagicks.common.lib.LibInfo;

import java.util.List;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 30, 2014 at 8:55 PM.
 */
public class ItemCivilTome extends Item {

    public ItemCivilTome() {
        super();
        this.setUnlocalizedName(LibInfo.MOD_ID + ":civiltome");
        this.setCreativeTab(CivilMagicks.tabCivV);
    }

    @Override
    public void onCreated(ItemStack is, World world, EntityPlayer player) {
        is.stackTagCompound = new NBTTagCompound();
//        is.stackTagCompound.setInteger("MUUID", player.);
        is.stackTagCompound.setString("master", player.getDisplayName());
    }

    @Override
    public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer player) {

        player.openGui(CivilMagicks.instance, LibGuiId.BOOK_CIVILTOME, player.worldObj,(int) player.posX, (int) player.posY, (int) player.posZ);
//        if(is.stackTagCompound != null) {
//            String master = is.stackTagCompound.getString("master");
//            if(master == player.getDisplayName()) {
//                System.out.println("FOOO");
//            } else {
//                System.err.println("YOU ARE NOT MY MASTER");
//            }
//        }
        return is;
    }



    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack is, EntityPlayer player, List list, boolean par4) {
        if (is.stackTagCompound !=null) {
            String master = is.stackTagCompound.getString("master");
            list.add(StatCollector.translateToLocal(EnumChatFormatting.DARK_PURPLE + "This tool is Bound to: " + EnumChatFormatting.DARK_AQUA + master));
        }
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister reg) {
        itemIcon = reg.registerIcon(LibInfo.MOD_ID +":civiltome");
    }
}
