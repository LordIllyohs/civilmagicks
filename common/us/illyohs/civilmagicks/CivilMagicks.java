package us.illyohs.civilmagicks;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraft.creativetab.CreativeTabs;

import us.illyohs.civilmagicks.client.handler.GuiHandler;
import us.illyohs.civilmagicks.common.blocks.ModBlocks;
import us.illyohs.civilmagicks.common.items.ModItems;
import us.illyohs.civilmagicks.common.lib.IProxy;
import us.illyohs.civilmagicks.common.lib.LibInfo;
import us.illyohs.civilmagicks.common.lib.TabCivilMagicks;

/**
 * @author LordIllyohs(Anthony Andersom)
 *         File created on May 30, 2014 at 7:41 PM.
 */

@Mod(modid = LibInfo.MOD_ID, name = LibInfo.MOD_NAME, version = LibInfo.MOD_VERSION)
public class CivilMagicks {

    @Mod.Instance
    public static CivilMagicks instance;

    public static CreativeTabs tabCivV = new TabCivilMagicks(LibInfo.MOD_ID + ":" + LibInfo.MOD_ID);

    @SidedProxy(clientSide = LibInfo.CLIENTPROXY, serverSide = LibInfo.COMMONPROXY)
    public static IProxy proxy;

    @Mod.EventHandler
    public void preinit(FMLPreInitializationEvent event) {
        ModBlocks.init();
        ModItems.init();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.renderModels();
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
    }

    @Mod.EventHandler
    public void postinit(FMLPostInitializationEvent event) {

    }

}
